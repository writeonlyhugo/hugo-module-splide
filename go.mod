module gitlab.com/writeonlyhugo/hugo-module-splide

go 1.17

require github.com/splidejs/splide v4.1.3+incompatible // indirect
